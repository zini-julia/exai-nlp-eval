# On the Evaluation of the Plausibility and Faithfulness of Sentiment Analysis Explanations 

In this project we evaluate the plausibility and faithfulness of explanations derived from 3 explainable AI methods on 8 sentiment analysis models with different architectures. 


Our paper is to be published in AIAI 2022.

# Dataset 
We consider the Rotten Tomatoes dataset with binary labels and we complement it with two additional labels: reasoning difficulty, and extracted rationales.

## Reasoning difficulty
It reflects the complexity of reasoning about the sentiment and takes three values: $1, 2$, and $3$ with 3 being the most difficult. A special tag $4$ is used when a sentence requires additional context understanding. For example, the following review is labeled as 4: ``not even the hanson brothers can save it''. Explaining this sentence would require a higher-level knowledge of what the reviewer meant by hanson brothers in a different context. The reasoning difficulty does not reflect the readability nor the semantic complexity. It rather shows the difficulty of extracting evidence of a particular sentiment. 

## Rationales 
They are a list of particular words in the sentence that contributed to the sentiment prediction. In curating the dataset, we alleviate human bias and subjectivity by asking more than one labeler to extract the rationales of each sentence. The aggregation of these explanations is done using the conjunction and disjunction operations. An additional correction step was applied to the aggregated explanations.

# Metrics 
We suggest 6 metrics inspired from information retrieval.
The metrics are explained in the paper

# Citation 
Please cite this work as
Julia El Zini, Mohamad Mansour, Basel Mouzi Basel and Mariette Awad, AIAI, 2022.

Contact author: Julia El Zini (jwe04@mail.aub.edu)
